import { dbQuery, syncDB } from "./utils/db"
import { random } from "faker"
import { get, post } from "request-promise-native"
import { expect } from "chai"

const TRACKER_HOST = process.env['TRACKER_HOST'] || 'localhost';
const TRACKER_PORT = process.env['TRACKER_PORT'] || 3000;

describe('Tracker Server', function() {
    this.timeout(30000);

    before(function(done){
        setTimeout(done, 15000);
    });

    beforeEach(async function() {
        await syncDB();

        // reset database
        await dbQuery({
            type: "remove",
            table: "track_events"
        });

        //masukin
        this.track = (await dbQuery({
            type: "insert",
            table: "track_events",
            values: {
                "rider_id": 4,
                "north": random.number({min:1, max: 20}),
                "east": random.number({min:1, max: 20}),
                "west": random.number({min:1, max: 20}),
                "south": random.number({min:1, max: 20}),
                "createdAt": new Date(),
                "updatedAt": new Date()
            }, 
            returning: [
                'rider_id',
                'north',
                'west',
                'south',
                'east',
                'createdAt'
            ]
        }))[0][0];

        // [ [ { rider_id: 4, west: 5 } ] ]
    })

    describe('Movement', function() {
        it('harusnya memberikan data suatu rider', async function() {
            const response = await get(
                `http://${TRACKER_HOST}:${TRACKER_PORT}/movement/4`, 
                {json: true}
            );
            expect(response.ok).to.be.true;
            expect(response.logs).to.be.deep.eq([{
                west: this.track.west,
                east: this.track.east,
                south: this.track.south,
                north: this.track.north,
                time: this.track.createdAt.toISOString()
            }])
        });

        it('harusnya menampilkan status ok: true', async function() {
            const response = await post(`http://${TRACKER_HOST}:${TRACKER_PORT}/track`, 
                { json: true, body: {rider_id: "1", north: "10", south: "0", west: "0", east: "0"} }
            );
            expect(response.ok).to.be.true;
        });
    })
})